import React,{Component} from 'react';
import CurrentBill from './currentBill';
import NavBar from './navBar';
import ProductList from './productList';
class MainComp extends Component{
    state={
        products:[
            {"code":"PEP221","prod":"Pepsi","price":12,"instock":"Yes","category":"Beverages"},
            {"code":"COK113","prod":"Coca Cola","price":18,"instock":"Yes","category":"Beverages"},
            {"code":"MIR646","prod":"Mirinda","price":15,"instock":"No","category":"Beverages"},
            {"code":"SLI874","prod":"Slice","price":22,"instock":"Yes","category":"Beverages"},
            {"code":"MIN654","prod":"Minute Maid","price":25,"instock":"Yes","category":"Beverages"},
            {"code":"APP652","prod":"Appy","price":10,"instock":"No","category":"Beverages"},
            {"code":"FRO085","prod":"Frooti","price":30,"instock":"Yes","category":"Beverages"},
            {"code":"REA546","prod":"Real","price":24,"instock":"No","category":"Beverages"},
            {"code":"DM5461","prod":"Dairy Milk","price":40,"instock":"Yes","category":"Chocolates"},
            {"code":"KK6546","prod":"Kitkat","price":15,"instock":"Yes","category":"Chocolates"},
            {"code":"PER5436","prod":"Perk","price":8,"instock":"No","category":"Chocolates"},
            {"code":"FST241","prod":"5 Star","price":25,"instock":"Yes","category":"Chocolates"},
            {"code":"NUT553","prod":"Nutties","price":18,"instock":"Yes","category":"Chocolates"},
            {"code":"GEM006","prod":"Gems","price":8,"instock":"No","category":"Chocolates"},
            {"code":"GD2991","prod":"Good Day","price":25,"instock":"Yes","category":"Biscuits"},
            {"code":"PAG542","prod":"Parle G","price":5,"instock":"Yes","category":"Biscuits"},
            {"code":"MON119","prod":"Monaco","price":7,"instock":"No","category":"Biscuits"},
            {"code":"BOU291","prod":"Bourbon","price":22,"instock":"Yes","category":"Biscuits"},
            {"code":"MAR951","prod":"MarieGold","price":15,"instock":"Yes","category":"Biscuits"},
            {"code":"ORE188","prod":"Oreo","price":30,"instock":"No","category":"Biscuits"}
        ],
        selected:"",
        categoryList:["Beverages","Chocolates","Biscuits"],
        stockList:["Yes","No"],
        priceList:["<10","10-20",">20"],
        filterProd:{category:"",stock:"",range:""},
        bill:[],
        view:0
    }
    handleView=(num)=>{
        let s1={...this.state};
        s1.view=num;
        this.setState(s1);
    }
    handleBill=(num,index)=>{
        let s1={...this.state};
        if(num===0){
            ++s1.bill[index].qty;
        }
        else if(num===1){
            if(s1.bill[index].qty===1){
                --s1.bill[index].qty;
                s1.bill.splice(index,1);
            }
            else{
                --s1.bill[index].qty;
            }
        }
        else if(num==2){
            s1.bill.splice(index,1);
        }
        else{
            s1.bill=[];
        }
        this.setState(s1);
    }
    getElements=(str,arr)=>{
        if(str.indexOf("-")!=-1){
            let nums=str.split("-");
            return arr.filter((x)=>{
                return x.price>=parseInt(nums[0])&&x.price<=parseInt(nums[1])
            })
        }
        else{
            let nums=str.split(str.charAt(0));
            if(str.charAt(0)===">"){
                return arr.filter((x)=>{
                    return x.price>parseInt(nums[1]);
                })  
            }
            else{
                return arr.filter((x)=>{
                    return x.price<parseInt(nums[1]);
                })
            }
        }
    }
    addToBill=(product)=>{
        let s1={...this.state};
        let json={...product};
        if(!s1.bill.length){
            json.qty=1;
            s1.bill.push(json);
        }
        else{
            let index=s1.bill.findIndex((x)=>x.code===product.code);
            if(index>=0){
                ++s1.bill[index].qty;
            }
            else{
                json.qty=1;
                s1.bill.push(json);
            }
        }
        this.setState(s1);
    }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s1={...this.state};
        s1.filterProd[input.name]=input.value;
        this.setState(s1);
    }
    sortArr=(num)=>{
        let s1={...this.state};
        if(num===0){
            s1.products.sort((x,y)=>x.code.localeCompare(y.code));
            s1.selected="Code";
        }
        else if(num===1){
            s1.products.sort((x,y)=>x.prod.localeCompare(y.prod));
            s1.selected="Product";
        }
        else if(num===2){
            s1.products.sort((x,y)=>x.price-y.price);
            s1.selected="Price";
        }
        else if(num===3){
            s1.products.sort((x,y)=>x.instock.localeCompare(y.instock));
            s1.selected="InStock";
        }
        else{
            s1.products.sort((x,y)=>x.category.localeCompare(y.category));
            s1.selected="Category";
        }
        this.setState(s1);
    }
    render(){
        let {products,view,bill,filterProd,categoryList,stockList,priceList,selected}=this.state;
        let {category,stock,range}=filterProd;
        let products2=category?products.filter((x)=>x.category===category):products;
        let products3=stock?products2.filter((x)=>x.instock===stock):products2;
        let products4=range?this.getElements(range,products3):products3;
        console.log(filterProd);
        return(
            <div className="container">
                <NavBar
                changeView={this.handleView}/>
                {
                    view===1?<React.Fragment>
                        <h2>Details Of Current Bill</h2>
                        <CurrentBill
                        bill={bill}
                        handleBill={this.handleBill}/>
                        <h3 className="text-center">Product List</h3>
                        <div className="row">
                            <div className="col-3"><b>Filer Product By:</b></div>
                            <div className="col-3">
                                <div className="form-group">
                                <select className="form-control" id="category" name="category" value={category} onChange={this.handleChange}>
                                <option selected value="">
                                    Select Category
                                </option>
                                {
                                    categoryList.map((x)=><option>{x}</option>)
                                }
                                </select>
                                </div>
                            </div>
                            <div className="col-3">
                                <div className="form-group">
                                <select className="form-control" id="stock" name="stock" value={stock} onChange={this.handleChange}>
                                <option selected value="">
                                    Select Stock
                                </option>
                                {
                                    stockList.map((x)=><option>{x}</option>)
                                }
                                </select>
                                </div>
                            </div>
                            <div className="col-3">
                                <div className="form-group">
                                <select className="form-control" id="range" name="range" value={range} onChange={this.handleChange}>
                                <option selected value="">
                                    Select Price Range
                                </option>
                                {
                                    priceList.map((x)=><option>{x}</option>)
                                }
                                </select>
                                </div>
                            </div>
                            
                        </div>
                        <div className="row bg-dark text-white">
                            <div className="col-2" onClick={()=>this.sortArr(0)}>{selected==="Code"?"Code(X)":"Code"}</div>
                            <div className="col-2" onClick={()=>this.sortArr(1)}>{selected==="Product"?"Product(X)":"Product"}</div>
                            <div className="col-2" onClick={()=>this.sortArr(2)}>{selected==="Price"?"Price(X)":"Price"}</div>
                            <div className="col-2" onClick={()=>this.sortArr(3)}>{selected==="InStock"?"InStock(X)":"InStock"}</div>
                            <div className="col-2" onClick={()=>this.sortArr(4)}>{selected==="Category"?"Category(X)":"Category"}</div>
                            <div className="col-2"></div>
                        </div>
                        {
                            products4.map((x,index)=><ProductList
                                    product={x}
                                    addToBill={this.addToBill}
                                    />
                                )
                        }
                    </React.Fragment>:""
                }
            </div>
        )
    }
}
export default MainComp