import React,{Component} from 'react';
class ProductList extends Component{
    render(){
        let {code,prod,price,instock,category}=this.props.product;
        return(
            <React.Fragment>
                <div className="row border p-2">
                    <div className="col-2">{code}</div>
                    <div className="col-2">{prod}</div>
                    <div className="col-2">{price}</div>
                    <div className="col-2">{instock}</div>
                    <div className="col-2">{category}</div>
                    <div className="col-2"><button className="btn btn-secondary"
                    onClick={()=>this.props.addToBill(this.props.product)}>Add To Bill</button></div>
                </div>
            </React.Fragment>
        )
    }
}
export default ProductList;