import React,{Component} from 'react';
class CurrentBill extends Component{
    getQunatity=()=>{
        let bill=this.props.bill;
        return !bill.length?0:bill.reduce((acc,curr)=>acc+=curr.qty,0);
    }
    getAmount=()=>{
        let bill=this.props.bill;
        return !bill.length?0:bill.reduce((acc,curr)=>acc+=curr.qty*curr.price,0);
    }
    render(){
        let bill=this.props.bill;
        return(
            <React.Fragment>
                <span>Items:{bill.length},Quantity:{this.getQunatity()},Amount:{this.getAmount()}</span>
                {bill.length?<React.Fragment>{bill.map((x,index)=>(
                    <div className="row border p-1">
                        <div className="col-6">{x.code+" "+x.prod+" Price : "+x.price+" Qunatity : "+x.qty+" Value : "+(x.price*x.qty)}</div>
                         <div className="col-6">
                         <button class="btn btn-success btn-sm" onClick={()=>this.props.handleBill(0,index)}>+</button>
                         <button class="btn btn-warning btn-sm" onClick={()=>this.props.handleBill(1,index)}>-</button>
                         <button class="btn btn-danger btn-sm" onClick={()=>this.props.handleBill(2,index)}>x</button>
                         </div>
                    </div>
                ))}<button className="btn btn-primary"
                onClick={()=>this.props.handleBill(3)}>Close Bill</button></React.Fragment>:""}
            </React.Fragment>
        )
    }
}
export default CurrentBill;