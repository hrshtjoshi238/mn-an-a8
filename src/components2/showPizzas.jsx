import React,{Component} from 'react';
class ShowPizza extends Component{
    state={
        pizza:this.props.pizza,
    }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s1={...this.state};
        s1.pizza[input.name]=input.value;
        this.setState(s1);
    }
    handleSubmit=()=>{
        if(!this.state.pizza.size&&this.props.product.type==="Pizza"){
            alert("Select the Size");
        }
        else if(!this.state.pizza.crust&&this.props.product.type==="Pizza"){
            alert("Select the Crust")
        }
        else{
            //console.log(this.props.product);
            this.props.add2Cart(this.state.pizza,this.props.product);
        }     
    }
    render(){
        let {size,crust}=this.state.pizza;
        let {cartItem,product,isPizza,sizes,crusts}=this.props;
        console.log(sizes);
        console.log(size+":"+crust);
        return(
            <div className="col-6 border text-center p-2">
                <img className="card-img-top" src={product.image}/>
                <b>{product.name}</b><br/>
                {product.desc}<br/>
                {isPizza?<div className="row p-3">
                    <div className="col-6">
                        <select className="form-control" value={size} name="size" onChange={this.handleChange} disabled={cartItem?"true":""}>
                            <option disabled value="">Select Size</option>
                            {
                                sizes.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    <div className="col-6">
                         <select className="form-control" value={crust} name="crust" onChange={this.handleChange} disabled={cartItem?"true":""}>
                            <option disabled value="">Select Crust</option>
                            {
                                crusts.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                </div>:""}
                <div className="ml-auto">{!cartItem?<button className="btn btn-primary btn-sm" onClick={this.handleSubmit}>Add To Cart</button>
                 :<React.Fragment><button class="btn btn-danger btn-sm" onClick={()=>this.props.handleCart(0,cartItem.id)}>-</button>
                 <button class="btn btn-secondary btn-sm" disabled="true">{cartItem.qty}</button>
                 <button class="btn btn-success btn-sm" onClick={()=>this.props.handleCart(1,cartItem.id)}>+</button></React.Fragment>}</div>
            </div>
        )
    }
}
export default ShowPizza;