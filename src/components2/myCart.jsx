import React,{Component} from 'react';
class MyCart extends Component{
    render(){
        let {cartItem}=this.props;
        console.log(cartItem.pizza);
        return(
            <div className="row border">
                <div className="col-4 p-4">
                    <img className="img-sm img-fluid" src={cartItem.image}/>
                </div>
                <div className="col-8">
                    <b>{cartItem.name}</b>
                    <p>{cartItem.desc}</p>
                    {
                        cartItem.type==="Pizza"?
                        <p><b>{cartItem.size}|{cartItem.crust}</b></p>:
                        ""
                    }
                    <button class="btn btn-danger btn-sm" onClick={()=>this.props.handleCart(0,cartItem.id)}>-</button>
                    <button class="btn btn-secondary btn-sm" disabled="true">{cartItem.qty}</button>
                    <button class="btn btn-success btn-sm" onClick={()=>this.props.handleCart(1,cartItem.id)}>+</button>
                </div>
            </div>
        )
    }
}
export default MyCart;