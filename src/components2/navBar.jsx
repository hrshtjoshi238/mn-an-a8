import React, {Component} from 'react';
class NavBar extends Component{
    render(){
        return(
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#">
                    MyFavPizza
                </a>
                <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-taget="#navbarSupportedContent">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto" href="#">
                        <li className="nav-item">
                            <a className="nav-link" href="#" onClick={()=>this.props.changeView(1)}>
                                Veg Pizza
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#" onClick={()=>this.props.changeView(2)}>
                                Non-Veg Pizza
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#" onClick={()=>this.props.changeView(3)}>
                                Side-Dishes
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#" onClick={()=>this.props.changeView(4)}>
                                Other Items
                            </a>
                        </li>
                    </ul>
                    
                </div>
            </nav>
        );
    }
}
export default NavBar;